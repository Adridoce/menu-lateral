package com.example.menudrawer.ui.home

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.menudrawer.*
import com.example.menudrawer.R
import com.example.menudrawer.adapter.ConsultaAdapter
import com.example.menudrawer.databinding.FragmentHomeBinding
import com.example.menudrawer.provider.Consulta
import com.github.florent37.runtimepermission.RuntimePermission
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var consultaArrayList: ArrayList<Consulta>
    private lateinit var consultaAdapter: ConsultaAdapter
    private lateinit var querysArrayList: ArrayList<String>
    private lateinit var preguntasArrayList: ArrayList<String>

    private lateinit var db: FirebaseFirestore

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        ViewModelProvider(this)[HomeViewModel::class.java]

        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        db = FirebaseFirestore.getInstance()

        binding.recyclerConsultas.layoutManager = LinearLayoutManager(context)
        binding.recyclerConsultas.setHasFixedSize(true)

        consultaArrayList = arrayListOf()

        consultaAdapter = ConsultaAdapter(consultaArrayList)
        binding.recyclerConsultas.adapter = consultaAdapter

        val btnCamara: FloatingActionButton? = binding.root.findViewById(R.id.btnCamara)

        btnCamara?.setOnClickListener {
            RuntimePermission.askPermission(this)
                .request(Manifest.permission.CAMERA)
                .onAccepted {
                    val intent = Intent(context, ScannerActivity::class.java)
                    startActivity(intent)
                }
                .onDenied {
                    Toast.makeText(context, "Por favor acepta los permisos", Toast.LENGTH_LONG).show()
                    it.askAgain()
                }
                .ask()
        }

        initRecyclerHome()

        return binding.root
    }


    private fun initRecyclerHome() {

        preguntasArrayList = arrayListOf(
            "¿Cuantas camisetas has comprado?",
            "¿Cuantos pantalones has comprado?",
            "¿Cuanto dinero has gastado?",
            "¿Cuantas compras has hecho en Zara?",
            "¿Cuantas compras has hecho en Pull and Bear?",
            "¿Cuantas compras has hecho en Bershka?",
            "¿Cual es tu tienda favorita?",
        )

        querysArrayList = arrayListOf(
            nCamisetas.toString(),
            nPantalones.toString(),
            dineroGastado.toString(),
            nVecesZara.toString(),
            nVecesPull.toString(),
            nVecesBershka.toString(),
            tiendaFav.toString(),
        )

        for ( i in preguntasArrayList.indices)
        {
            consultaArrayList.add(Consulta(preguntasArrayList[i], querysArrayList[i]))
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}