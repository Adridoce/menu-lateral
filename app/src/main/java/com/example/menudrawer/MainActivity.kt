package com.example.menudrawer

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.menudrawer.adapter.ConsultaAdapter
import com.example.menudrawer.databinding.ActivityMainBinding
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.getField
import java.lang.Exception
import java.lang.NumberFormatException

var nVecesZara: String? = "0"
var nVecesPull: String? = "0"
var nVecesBershka: String? = "0"
var nCamisetas: String? = "0"
var nPantalones: String? = "0"
var dineroGastado: Double? = 0.0
var tiendaFav: String? = "0"


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var firebaseAuth: FirebaseAuth
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(2000)
        setTheme(R.style.AppTheme)

        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)

        firebaseAuth = FirebaseAuth.getInstance()

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_content_main)

        appBarConfiguration = AppBarConfiguration(navController.graph, drawerLayout)

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        datosUsuario()
        datosConsultas()

    }




    private fun datosConsultas() {
        comprasCamisetas()
        comprasPantalones()
        dineroGastado()
        comprasZara()
        comprasPull()
        comprasBershka()
        tiendaFav()
    }

    private fun datosUsuario() {
        val user = firebaseAuth.currentUser
        val email = user?.email
        var nombreUsuario = ""

        val navView: NavigationView = binding.navView
        val headerView = navView.getHeaderView(0)
        val userNameTextView = headerView.findViewById(R.id.tvUsuario) as TextView
        val userEmailTextView = headerView.findViewById(R.id.tvEmailUsuario) as TextView

        db.collection("users").document(email.toString()).get().addOnSuccessListener { documento ->
            nombreUsuario = documento.data?.get("nombre").toString() + " " + documento.data?.get("apellido")
                    .toString()

            userNameTextView.text = nombreUsuario
            userEmailTextView.text = email.toString()
        }
    }


    private fun comprobarUsuario() {
        val user = firebaseAuth.currentUser

        if (user != null)
        {
            val email = user.email
        }
        else
        {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    // Menu superior
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }


    private fun comprasCamisetas() {

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user!!.email
        var localnCamisetas = 0
        var unidades = 0

        nCamisetas = ""

        db.collection("users").document(email.toString()).collection("productos")
            .whereEqualTo("producto", "camiseta").get()
            .addOnSuccessListener { documents ->
                for (document in documents)
                {
                    unidades = document.getField("unidades")!!
                    localnCamisetas += unidades
                    nCamisetas = localnCamisetas.toString()
                }
            }

    }


    private fun comprasPantalones() {

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user!!.email
        var localnPantalones = 0
        var unidades = 0

        nPantalones = ""

        db.collection("users").document(email.toString()).collection("productos")
            .whereEqualTo("producto", "pantalon").get()
            .addOnSuccessListener { documents ->
                for (document in documents)
                {
                    unidades = document.getField("unidades")!!
                    localnPantalones += unidades
                    nPantalones = localnPantalones.toString()
                }

            }


    }

    private fun dineroGastado () {

        try {

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user!!.email
        var unidades = 0
            var total = 0.0
            var totalDinero = 0.0
            var final = 0.0

            dineroGastado = 0.0

            db.collection("users").document(email.toString())
                .collection("productos").get()
                .addOnSuccessListener { documents ->
                    for (document in documents)
                    {
                        unidades = document.getLong("unidades")!!.toInt()
                        totalDinero =  (document.getDouble("precio")!!)
                        total  = totalDinero!! * unidades
                        final =  final + total
                    }
                    dineroGastado = final
                }

        }catch (e:RuntimeException){
            Toast.makeText(this, "Error numérico", Toast.LENGTH_SHORT).show()
        }catch (e:Exception){
            Toast.makeText(this, "Error ", Toast.LENGTH_SHORT).show()
        }

    }

    private fun comprasZara() {

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user!!.email
        var localnVeces = 0
        nVecesZara = ""
        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "zara").get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    localnVeces += 1
                    nVecesZara = localnVeces.toString()
                }
            }

    }

    private fun comprasPull() {

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user!!.email
        var localnVeces = 0

        nVecesPull = ""
        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "pull and bear").get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    localnVeces += 1
                    nVecesPull = localnVeces.toString()
                }
            }
    }

    private fun comprasBershka() {

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user!!.email
        var localnVeces = 0

        nVecesBershka = ""
        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "bershka").get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    localnVeces += 1
                    nVecesBershka = localnVeces.toString()
                }
            }
    }

    private fun tiendaFav() {

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user!!.email
        var nComprasZara = 0
        var nComprasPull = 0
        var nComprasBershka = 0
        var numMayor = 1

        tiendaFav = ""

            db.collection("users").document(email.toString()).collection("tickets")
                .whereEqualTo("tienda", "zara").get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        nComprasZara += 1
                    }
                    if (nComprasZara >= numMayor && numMayor != 0) {
                        numMayor = nComprasZara
                        tiendaFav = "Zara"

                    }

                }

            db.collection("users").document(email.toString()).collection("tickets")
                .whereEqualTo("tienda", "pull and bear").get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        nComprasPull += 1
                    }
                    if (nComprasPull >= numMayor && numMayor != 0) {
                        numMayor = nComprasPull
                        tiendaFav = "Pull & Bear"

                    }

                }
            db.collection("users").document(email.toString()).collection("tickets")
                .whereEqualTo("tienda", "bershka").get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        nComprasBershka += 1 //c
                    }
                    if (nComprasBershka >= numMayor && numMayor != 0) {
                        numMayor = nComprasBershka
                        tiendaFav = "Bershka"

                    }

                }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.menuLogOut -> {
                this.firebaseAuth.signOut()
                this.comprobarUsuario()
                dineroGastado = 0.0
                nVecesZara = ""
                nVecesPull = ""
                nVecesBershka = ""
                tiendaFav = ""
                nCamisetas = ""
                nPantalones = ""
            }

            R.id.menuActualizar -> {
                val refresh =
                    Intent(this, MainActivity::class.java)
                startActivity(refresh)
                this.finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }
}


