package com.example.menudrawer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.menudrawer.R
import com.example.menudrawer.provider.Consulta

class ConsultaAdapter(private val consultaList:List<Consulta>) : RecyclerView.Adapter<ConsultaAdapter.viewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return viewHolder(layoutInflater.inflate(R.layout.item_consulta, parent, false))

    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {

        val consulta = consultaList[position]

        holder.consulta.text = consulta.consulta
        holder.respuesta.text = consulta.respuesta
    }

    override fun getItemCount(): Int {

        return consultaList.size

    }

    class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val consulta: TextView = itemView.findViewById(R.id.tvConsulta)
        val respuesta: TextView = itemView.findViewById(R.id.tvRespuesta)

    }


}