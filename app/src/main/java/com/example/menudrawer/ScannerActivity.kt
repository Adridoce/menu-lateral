package com.example.menudrawer

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.menudrawer.botones.TicketBershka
import com.example.menudrawer.botones.TicketPullAndBear
import com.example.menudrawer.botones.TicketZara
import com.example.menudrawer.databinding.ActivityScannerBinding
import com.lucem.anb.characterscanner.Scanner
import com.lucem.anb.characterscanner.ScannerListener

//DECLARAR VARIABLE GLOBAL
var a:String ?= null

class ScannerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityScannerBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityScannerBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        var extractValue:String?= ""
            //Esconder los botones de las tiendas
            binding.btnZara.visibility = View.GONE
            binding.btnPullBear.visibility = View.GONE
            binding.btnBershka.visibility = View.GONE

        val scanner = Scanner(this,binding.surfaceView,object : ScannerListener {
            override fun onDetected(texto: String?) {
                extractValue = texto

            }

            override fun onStateChanged(texto: String?, p1: Int) {
                if (texto != null) {
                    Log.d("check state", texto)
                }
            }

        })


        //Al pulsar al boton comprueba que se haya escaneado y mantiene visible el editor de texto.
        binding.btnCapture.setOnClickListener {
            // al pulsar el boton capturar de dejan de ver Surface y el boton y aparace Edit(text)
            // Muestra los botones y esconde el de escanear
            scanner.isScanning = false
            binding.surfaceView.visibility = View.GONE
            binding.btnCapture.visibility = View.GONE
            binding.btnZara.visibility = View.VISIBLE
            binding.btnPullBear.visibility = View.VISIBLE
            binding.btnBershka.visibility = View.VISIBLE
            binding.edtValue.visibility = View.VISIBLE
            binding.edtValue.setText(extractValue)


        }

        binding.btnZara.setOnClickListener {
            a = extractValue
            if (a!!.contains("ZARA")) {
                val intent = Intent(this, TicketZara::class.java)
                startActivity(
                    intent
                )
            } else {
                Toast.makeText(this, "Este tickets no es del ZARA", Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnPullBear.setOnClickListener {
            a = extractValue
            if (a!!.contains("PULL")) {
                val intent = Intent(this, TicketPullAndBear::class.java)
                startActivity(
                    intent
                )
            } else {
                Toast.makeText(this, "Este tickets no es del PULL AND BEAR", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        binding.btnBershka.setOnClickListener {
            a = extractValue
            if (a!!.contains("BERSHKA")) {
                val intent = Intent(this, TicketBershka::class.java)
                startActivity(
                    intent
                )
            } else {
                Toast.makeText(this, "Este tickets no es del BERSHKA", Toast.LENGTH_SHORT).show()
            }
        }
    }

        override fun onBackPressed() {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("¿Esta seguro de eliminar?")
            builder.setMessage("Aviso")
            builder.setPositiveButton("Si") {
                    dialogInterface, i -> dialogInterface.dismiss()
                    super.onBackPressed()
            }

            builder.setNegativeButton("No") { dialogInterface, i ->
                dialogInterface.dismiss()
            }

            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        }

}