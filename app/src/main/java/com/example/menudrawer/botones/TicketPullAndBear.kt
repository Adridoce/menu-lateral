package com.example.menudrawer.botones

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.menudrawer.MainActivity
import com.example.menudrawer.R
import com.example.menudrawer.ScannerActivity
import com.example.menudrawer.a
import com.example.menudrawer.databinding.ActivityTicketPullandbearBinding

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class TicketPullAndBear : AppCompatActivity() {
    var parametros = ArrayList<TicketZara.producto>()

    private lateinit var firebaseAuth: FirebaseAuth
    private val db = FirebaseFirestore.getInstance()
    private lateinit var binding: ActivityTicketPullandbearBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityTicketPullandbearBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonMostrarDatos.setOnClickListener {
            SacarTexto(
                TicketZara.producto(),
                TicketZara.datosTienda(tienda = "", fecha = "", precioTotal = "")
            )
            binding.buttonGuardar.visibility = View.VISIBLE
            binding.buttonRepetir.visibility = View.VISIBLE
            binding.Pregunta.visibility = View.VISIBLE
            binding.buttonMostrarDatos.visibility = View.INVISIBLE

        }

        Repetir()
        btnVolver()
        Guardar(
            TicketZara.producto(),
            TicketZara.datosTienda(tienda = "", fecha = "", precioTotal = "")
        )
    }

    fun SacarTexto(producto: TicketZara.producto, dt: TicketZara.datosTienda) {

        try {

            val SalidatextoProductos = binding.textViewMProducto
            val SalidatextoPrecioIndividual = binding.textViewMPrecio
            val SalidatextoUnidades = binding.textViewMUnidades
            val SalidatextoTienda = binding.textViewMTienda
            val SalidatextoFechaCompra = binding.textViewMFecha
            val SalidatextoPrecioTotal = binding.textViewMPrecioTotal

            //El separador que tiene
            val delim = " "
            val delim2 = "\n"
            //convierto en split
            val list = a!!.split(delim, delim2)
            var aux: String = ""
            var aux2: String = ""
            var auxPrendas: String = ""
            var auxPrecioIndividual: String = ""
            var auxUnidades: String = ""
            var auxFecha: String = ""
            var auxSacarTextoPrecioTotal: String = ""
            var auxPrecioTotal: String = ""


            //Para que no tenga espacios y se pongan ,
            for (cero in list - 1) {
                //Busco la palabra importe y saco el texto hasta la palabra total.

                if (aux.contains("PULL")) {
                    SalidatextoTienda.text = "Pull and Bear"
                    dt.tienda = SalidatextoTienda.text.toString()

                }

                if (aux.contains("Expedición/0peración:")) {
                    if (cero.toString() != "TSP") {
                        auxFecha = auxFecha + " " + cero.toString()
                        dt.fecha = auxFecha
                    } else
                        aux = ""
                }

                if (aux.contains("IMPORTE")) {
                    if (cero.toString() != "Total") {
                        aux2 = aux2 + " " + cero.toString()

                    } else
                        aux = ""
                }


                if (aux.contains("TOTAL")) {
                    if (cero.toString() != "Efectivo" || cero.toString() != "Tarjeta") {
                        auxSacarTextoPrecioTotal = auxSacarTextoPrecioTotal + " " + cero.toString()

                    } else
                        aux = ""
                }
                aux += cero.toString()
            }
            //He sacado un texto y ese texto lo he convertido en un array
            val prenda = aux2.split(delim, delim2)
            val TextoPrecioTotal = auxSacarTextoPrecioTotal.split(delim, delim2)

            for (char in TextoPrecioTotal.indices) {
                if (char == 2) {
                    if (TextoPrecioTotal[char] != "\n") {
                        auxPrecioTotal += " " + TextoPrecioTotal[char]
                        dt.precioTotal = auxPrecioTotal
                        producto.precioTotal = dt.precioTotal.toDouble()

                    }
                }
                SalidatextoPrecioTotal.text = auxPrecioTotal
            }
            for (char in prenda.indices) {

                //UNIDADES
                if (char % 9 == 4) {
                    if (prenda[char] != "\n") {
                        auxUnidades += " " + prenda[char]
                        producto.nombreTienda = dt.tienda
                        producto.fechaCompra = dt.fecha
                        producto.unidad = prenda[char].toInt()
                    }
                }
                //PRECIO INDIVIDUAL
                if (char % 9 == 5) {
                    if (prenda[char] != "\n") {
                        auxPrecioIndividual += " " + prenda[char]
                        producto.precio = prenda[char].toDouble()
                    }
                }
                //PRENDAS
                if (char % 9 == 1 && char > 1) {
                    if (prenda[char] != "\n") {
                        auxPrendas += " " + prenda[char]
                        producto.Producto = prenda[char]

                        parametros.add(
                            TicketZara.producto(
                                producto.nombreTienda.lowercase(),
                                producto.fechaCompra.lowercase(),
                                producto.Producto.lowercase(),
                                producto.unidad,
                                producto.precio,
                                producto.precioTotal
                            )
                        )
                    }
                }
                SalidatextoProductos.text = auxPrendas
                SalidatextoPrecioIndividual.text = auxPrecioIndividual
                SalidatextoUnidades.text = auxUnidades
                SalidatextoFechaCompra.text = auxFecha

            }
        } catch (e: NumberFormatException) {
            val intent = Intent(this, ScannerActivity::class.java)
            startActivity(
                intent
            )
            Toast.makeText(this, "Error en el formato del numero", Toast.LENGTH_SHORT).show()
        }
    }


    fun Guardar(producto: TicketZara.producto, dt: TicketZara.datosTienda) {
        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user!!.email.toString().trim()
        var id = ""
        var funciona = 0
        var idProd = 0
        var numAdvertencia = 0
        var idTicket = ""

        db.collection("users").document(email).collection("productos").get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    idProd += 1
                }
            }
        db.collection("users").document(email).collection("tickets").get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    id = document.reference.id

                }
                if(id!= "") {
                    id.split(" ")
                    idTicket = id[7].toString()
                }else {
                    id = "ticket 0"
                    idTicket = id[7].toString()
                }
            }

                binding.buttonGuardar.setOnClickListener {


                    for (producto in parametros.indices) {
                        if (parametros[producto].fechaCompra.isEmpty() || parametros[producto].nombreTienda.isEmpty()
                            || parametros[producto].precioTotal == null || parametros[producto].Producto.isEmpty()
                            || parametros[producto].unidad == null || parametros[producto].precio == null
                        ) {
                            Fallo()
                        } else {
                            if (producto == 0) {
                                db.collection("users").document(email)
                                    .collection("tickets").document("ticket ${idTicket.toInt() + 1}")
                                    .set(
                                        hashMapOf(
                                            "id" to  (idTicket.toInt() + 1),
                                            "tienda" to parametros[producto].nombreTienda.lowercase(),
                                            "fecha" to parametros[producto].fechaCompra.lowercase(),
                                            "precioTotal" to parametros[producto].precioTotal
                                        )
                                    )
                                funciona++
                            }
                            db.collection("users").document(email)
                                .collection("productos").document("producto ${idProd + 1}")
                                .set(
                                    hashMapOf(
                                        "id" to idProd + 1,
                                        "idTicket" to  (idTicket.toInt() + 1),
                                        "producto" to parametros[producto].Producto.lowercase(),
                                        "unidades" to parametros[producto].unidad,
                                        "precio" to parametros[producto].precio
                                    )
                                )

                        }
                        idProd ++
                    }
                    funciona++

                    if (funciona == 2) {
                        Toast.makeText(
                            this,
                            "Se ha guardado correctamente.",
                            Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(
                            intent
                        )
                    }
                }
    }


    fun Repetir() {
        binding.buttonRepetir.setOnClickListener {
            a = ""
            val intent = Intent(this, ScannerActivity::class.java)
            startActivity(
                intent
            )
        }
    }

    //Funcion para volver a repetir si algún dato no esta relleno
    fun Fallo() {
        val dialogo = AlertDialog.Builder(this)
        dialogo.setTitle("Importante")
        dialogo.setMessage("Hay datos que no estan guardados correctamente, debe repetir la operación")
        dialogo.setPositiveButton("Aceptar") { dialogInterface, i ->
            dialogInterface.dismiss()
            val intent = Intent(this, ScannerActivity::class.java)
            startActivity(
                intent
            )
        }
        val alertDialog: AlertDialog = dialogo.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
    fun btnVolver(){
        binding.buttonSalir.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(
                intent
            )
        }
    }
}