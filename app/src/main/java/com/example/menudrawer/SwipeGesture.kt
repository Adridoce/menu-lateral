package com.example.menudrawer

import android.content.Context
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

abstract class SwipeGesture(context: Context?) : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

    val deleteColor = ContextCompat.getColor(context!!, R.color.deletecolor)
    val archiveColor = ContextCompat.getColor(context!!, R.color.archivecolor)
    val deleteIcon = R.drawable.ic_baseline_delete_24
    val archiveIcon = R.drawable.ic_baseline_archive_24

    val blanco = ContextCompat.getColor(context!!, R.color.white)


    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            .addSwipeLeftBackgroundColor(deleteColor).addSwipeLeftActionIcon(deleteIcon)
            .addSwipeLeftLabel("Eliminar").setSwipeLeftLabelTextSize(1, 18F)
            .setSwipeLeftActionIconTint(blanco).setSwipeLeftLabelColor(blanco)
            .addSwipeRightBackgroundColor(archiveColor).addSwipeRightActionIcon(archiveIcon)
            .addSwipeRightLabel("Archivar").setSwipeRightLabelTextSize(1,18F)
            .setSwipeRightActionIconTint(blanco).setSwipeRightLabelColor(blanco)
            .create().decorate()


        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }


}